import React from 'react'
import styled from 'styled-components'

import coloredLogo from 'assets/images/bigoDINHO.png'
import { mediaQueryUpTo } from 'app/helpers/theme'

const Logo = ({ ...otherProps }) => (
  <img
    src={coloredLogo}
    alt='dinho'
    {...otherProps}
  />
)

Logo.defaultProps = {
  variant: 'colored',
}

export default styled(Logo)`
  ${mediaQueryUpTo('xSmall', `max-width: 140px;`)}
`
