import React from 'react'
import PropTypes from 'prop-types'
import { withBreakpoints } from 'react-breakpoints'

import Button from 'app/components/core/Button'
import Text from 'app/components/core/Text'
import Logo from 'app/components/core/Logo'
import Menu from 'app/components/core/Menu'

const PublicMenu = ({
  text,
  href,
  buttonType,
  buttonText,
  className,
}) => (
  <div className={className}>
    <Menu
      leftSide={
        <Logo />
      }
      rightSide={
        <div>
          <Text variant='span' small>
            {text}
          </Text>

          <Button variant={buttonType} >{buttonText}</Button>
        </div>
      }
    />
  </div>
)

PublicMenu.propTypes = {
  text: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
  buttonType: PropTypes.string,
  buttonText: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
}

PublicMenu.defaultProps = {
  buttonType: 'outlined',
}

export default withBreakpoints(PublicMenu)
