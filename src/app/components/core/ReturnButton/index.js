import styled from 'styled-components'

import { getColor } from 'app/helpers/theme'

import Component from './component'

export default styled(Component)`
  text-align: left;
  opacity: 0.7;
  color: ${getColor('classical_burglar')};
  cursor: pointer;

  & svg {
    padding-right: 15px;
  }
`
