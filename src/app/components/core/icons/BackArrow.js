// <
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons'

const Icon = (props) => (
  <FontAwesomeIcon icon={faChevronLeft} {...props} />
)

export default Icon
