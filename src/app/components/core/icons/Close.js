// X
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

const Icon = (props) => (
  <FontAwesomeIcon icon={faTimes} {...props} />
)

export default Icon
