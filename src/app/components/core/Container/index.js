import styled from 'styled-components'

import Container from './component'

export default styled(Container)`
  max-width: 1140px;
  margin: 0 auto;
`
