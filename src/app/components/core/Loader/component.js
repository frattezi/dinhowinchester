import React from 'react'
import PropTypes from 'prop-types'

import Text from 'app/components/core/Text'

import tetris from 'assets/images/tetris-loader.gif'

const Loader = ({ className, variant }) => (
  <div className={className}>
    <div>
      <img src={tetris} alt='Loading...' />

      <Text>
        Loading...
      </Text>
    </div>
  </div>
)

Loader.propTypes = {
  variant: PropTypes.string,
  className: PropTypes.string.isRequired,
}

export default Loader
