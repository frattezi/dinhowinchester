import styled from 'styled-components'

import Component from './component'

export default styled(Component)`
  align-content: flex-end;
  text-align: end;
  padding: 40px 20px;
`
