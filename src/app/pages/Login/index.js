import React from 'react'
import PropTypes from 'prop-types'

import useAuth from 'app/redux/auth'
import Menu from 'app/components/core/PublicMenu'
import { Container } from './styled'

const Login = ({ login }) => (
  <Container >
    <Menu text='Dinho Winchester' buttonText='Saiba Mais' />
  </Container>
)

Login.propTypes = {
  login: PropTypes.func.isRequired,
}

export default useAuth(Login)
